# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

## [v0.0.2]

## v0.0.1
* First release.

[Unreleased]: https://codeberg.org/miurahr/omegat-new-nimbus/compare/v0.0.2...HEAD
[v0.0.2]: https://codeberg.org/miurahr/omegat-new-nimbus/compare/v0.0.1...v0.0.2
