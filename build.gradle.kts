plugins {
    java
    checkstyle
    distribution
    id("org.omegat.gradle") version "1.5.9"
}

version = "0.0.2"

repositories {
    mavenLocal()
}

omegat {
    version = "5.7.1"
    pluginClass = "org.omegat.gui.theme.NewNimbus"
}

checkstyle {
    isIgnoreFailures = true
    toolVersion = "7.1"
}

distributions {
    main {
        contents {
            from(tasks["jar"], "README.md", "COPYING", "CHANGELOG.md")
        }
    }
}
