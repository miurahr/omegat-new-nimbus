# OmegaT New Nimbus theme plugin

This is OmegaT theme plugin to provide new theme based on Nimbus Look and Feel for OmegaT 5.6.0 and later.

## Download

You can download plugin jar file from [Releases](https://codeberg.org/miurahr/omegat-new-nimbus/releases).

## Install

Please place the plugin jar file in OmegaT plugins folder where it is vary against OS you use.


## License

The plugin skeleton is distributed under GNU General Public License 3 or later.

